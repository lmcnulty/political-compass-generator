// Save the compass as an image.
function save() {
	html2canvas(document.getElementById("compass")).then(function(canvas) {
		var image = canvas.toDataURL("image/png");
		saveAs(image, "compass.png");
	});
}

for (input of document.querySelectorAll('input[type=file]')) {
	input.onchange = (evt) => {
		var preview = new Image();
		var file = evt.target.files[0];
		var reader= new FileReader();

		reader.onloadend = function () {
			preview.src = reader.result;
			console.log(preview);
			selectedQuadrant.appendChild(preview);
		}
		if (file) { reader.readAsDataURL(file); } 
		else      { preview.src = ""; }
	}
}

// Select quadrant when clicked, update inputs to selected quadrant values
selectedQuadrant = null;
for (quadrant of document.querySelectorAll(".quadrant")) {
	quadrant.style.fontSize = "18pt";
	quadrant.style.fontFamily = "sans-serif";
	quadrant.onclick = (evt) => {
		evt.target.focus();
		selectedQuadrant = evt.target;
		document.getElementById("font-size").value 
			= selectedQuadrant.style.fontSize.replace("pt", "");

		let ff = document.getElementById("font-face");
		for (let i = 0; i < ff.options.length; i++) {
			if (ff.options[i].value.replace(/'/g, '"') == selectedQuadrant.style.fontFamily) {
				ff.selectedIndex = i;
			}
		}
	}
}

function clearQuadrant() {
	selectedQuadrant.innerHTML = "";
}

// Update the font when user changes selection
document.getElementById("font-size").addEventListener("change", (evt) => {
	if (document.getElementById("same-font").checked) {
		console.log("same-font checked");
		for (quadrant of document.querySelectorAll(".quadrant")) {
			quadrant.style.fontSize = evt.target.value + "pt";
		}
	} else {
		selectedQuadrant.style.fontSize = evt.target.value + "pt";
	}
}, false);
document.getElementById("font-face").addEventListener("click", (evt) => {
	if (document.getElementById("same-font").checked) {
		console.log("same-font checked");
		for (quadrant of document.querySelectorAll(".quadrant")) {
			quadrant.style.fontFamily = evt.target.value;
		}
	} else {
		console.log("same-font not checked");
		selectedQuadrant.style.fontFamily = evt.target.value;
	}
}, false);


// Show the lib-right quadrant in purple when the box is checked.
let purpleLibRight = document.getElementById("purple-lib-right");
function updatePurple() {
	if (purpleLibRight.checked) {
		document.querySelector(".lib.right").style.backgroundColor 
			= "#E1C7E0";
	} else {
		document.querySelector(".lib.right").style.backgroundColor 
			= "#F5F4AB";
	}
}
purpleLibRight.addEventListener("click", updatePurple, false);
updatePurple();


// Show center quadrant when box is checked.
let includeCenter = document.getElementById("include-center");
function updateCenter() {
	if (includeCenter.checked) {
		console.log("includeCenter checked");
		document.getElementById("centrist").href = "centrist.css";
	} else {
		document.getElementById("centrist").href = "";
	}
};
includeCenter.addEventListener("click", updateCenter, false);
updateCenter();


// Use the same font in all quadrants when the box is checked.
function updateFont() {
	for (quadrant of document.querySelectorAll(".quadrant")) {
		quadrant.style.fontFamily 
			= document.getElementById("font-face").value;
		quadrant.style.fontSize
			= document.getElementById("font-size").value;
	}
}
document.getElementById("same-font")
	.addEventListener("click", updateFont, false);
updateFont();
